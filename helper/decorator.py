# Intro to Decorator
from datetime import datetime
from functools import wraps
import random
import timeit


# Python functions are First Class Citizen
def say_hello(name):
    return "hello {}".format(name)


def be_awesome(name):
    return "Yo {}, together we are awesome".format(name)


def greet_bob(greet_func):
    return greet_func("Bob")

# Now greet can take a function as argument and use Bob as
# parameter in the inner function


# === Example ===
greet_bob(say_hello)
greet_bob(be_awesome)
'''
output:
'hello Bob'
'Yo Bob, together we are awesome'
'''


# Function can be use inside of another function
# here wrapper is wrapping arg function with print
# and as the return is wrapper the output will be whole
# control flow of wrapper
def a_decorator(func):
    def wrapper():
        print("Something is happening before the function call")
        func()
        print("Something is happening after the function call")
    return wrapper


def a_func():
    print("It is Me.")


# decorators wrap a function, modifying its behavior
output = a_decorator(a_func)
output()


# === Example ===
def game_controller(func):
    def wrapper():
        if 7 <= datetime.now().hour < 22:
            func()
        else:
            print("It is night, No game now !")
            pass
    return wrapper


def play_game():
    print("Lets Play KOF")


# === Application ===
game_god = game_controller(play_game)
game_god()


# Simplify the decorators for reuse
def instruct_candidate(func):
    def wrapper():
        if 7 <= datetime.now().hour < 22:
            func()
        else:
            print("Go to bed, activity not allowed!")
            pass
    return wrapper


# Instead of instruct_candidate(watch_movie)
@instruct_candidate
def watch_movie():
    print("I can watch movie")


@instruct_candidate
def play_game():
    print("I can play football!")


watch_movie()
play_game()
'''
Output:
Go to bed, activity not allowed!
'''


# If we want to add an argument into the outer most function
# the arg must flow through the wrapper function
# So wrapper function also need the argument
# As we will not know how may argument we are going to pass
# it is better to use *arg, *karg
def instruct_candidate(func):
    def wrapper(*args, **kwargs):
        if 7 <= datetime.now().hour < 22:
            func(*args, **kwargs)
        else:
            print("Go to bed, activity not allowed!")
            pass
    return wrapper


@instruct_candidate
def play_game(game_name):
    print("I want to play {}".format(game_name))


# Test
play_game("Video Game")


# As the main function return the wrapper and
# Wrapper is just printing so the final return is None
# Which we can fix using return in wrapper
def instruct_candidate(func):
    def wrapper(*args, **kwargs):
        if 7 <= datetime.now().hour < 22:
            return func(*args, **kwargs)
        else:
            print("Go to bed, activity not allowed!")
            return False
    return wrapper


@instruct_candidate
def play_game(game_name):
    print("I want to play {}".format(game_name))
    return True


play_game()
'''
As it also change the identity of actual function.
play_game.__name__
'wrapper'
'''


# We can fix that issues using functools wrap
def instruct_candidate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if 7 <= datetime.now().hour < 22:
            return func(*args, **kwargs)
        else:
            print("Go to bed, activity not allowed!")
            return False
    return wrapper


@instruct_candidate
def play_game(game_name):
    print("I want to play {}".format(game_name))
    return True


play_game("Badminton")
'''
Fix the namespace
play_game.__name__
'play_game'
'''


# === Example ===
# Execution of a Loop function
def time_machine(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = timeit.timeit()
        result = func(*args, **kwargs)
        end = timeit.timeit()
        print("Time Taken {}".format(end - start))
        return result
    return wrapper


# Extending the simulation with time machine
@time_machine
def a_simulation_loop(n_iter: int, chunk_size: int):
    sum_list = []
    for i in range(n_iter):
        total = sum([int(1./random.random()) for i in range(chunk_size)])/chunk_size
        sum_list.append(total)
    return sum(sum_list)/n_iter


# Test
a_simulation_loop(10, 500000)
a_simulation_loop(50, 500000)
a_simulation_loop(100, 500000)
a_simulation_loop(500, 500000)
